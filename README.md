# Metlink fares

This is a unofficial version of fares section of the Wellington Metlink 
website and should not be relied on for accurate information. The official 
website is available at http://metlink.org.nz.

Fares information on this website was valid as at November 2016. Running 
test version can be found at [http://metlinkwellington.myvnc.com/](http://metlinkwellington.myvnc.com/) 
although this could go down anytime.

This is a python3 flask project, self contined within the project 
folder. Although it can also be run within Docker as follows:

To run in docker, first build the image:

	docker build -t alaw005/metlink .

Then run container

	docker run -d -p 5000:5000 --name metlink alaw005/metlink

You can now access from:

	http://[hostip]:5000