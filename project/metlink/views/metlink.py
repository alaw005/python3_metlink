from flask import Blueprint, render_template
from metlink import app

metlink = Blueprint('metlink', __name__, template_folder='templates')

@app.route('/<path:path>')
def static_file(path):
    return app.send_static_file(path)
    
@metlink.route('/')    
@metlink.route('/tickets-and-fares/')
def tickets_and_fares():
    
    fare_table = [
                    {"zones": "1", "adult_cash": "$2.00","adult_prepaid": "$1.66", "adult_month": "$49.80","child_cash": "$1.50", "child_prepaid": "$1.24","child_month": "$37.20"}, 
                    {"zones": "2", "adult_cash": "$3.50","adult_prepaid": "$2.73", "adult_month": "$81.90","child_cash": "$2.00", "child_prepaid": "$1.59","child_month": "$47.70"}, 
                    {"zones": "3", "adult_cash": "$5.00","adult_prepaid": "$3.63", "adult_month": "$108.90","child_cash": "$2.50", "child_prepaid": "$1.90","child_month": "$57.00"}, 
                    {"zones": "4", "adult_cash": "$5.50","adult_prepaid": "$4.08", "adult_month": "$122.40","child_cash": "$3.00", "child_prepaid": "$2.33","child_month": "$69.90"}, 
                    {"zones": "5", "adult_cash": "$6.50","adult_prepaid": "$4.98", "adult_month": "$149.40","child_cash": "$3.50", "child_prepaid": "$2.75","child_month": "$82.50"}, 
                    {"zones": "6", "adult_cash": "$8.00","adult_prepaid": "$6.33", "adult_month": "$189.90","child_cash": "$4.00", "child_prepaid": "$3.17","child_month": "$95.10"}, 
                    {"zones": "7", "adult_cash": "$9.00","adult_prepaid": "$7.18", "adult_month": "$215.40","child_cash": "$5.00", "child_prepaid": "$3.64","child_month": "$109.20"}, 
                    {"zones": "8", "adult_cash": "$10.50","adult_prepaid": "$8.03", "adult_month": "$240.90","child_cash": "$5.50", "child_prepaid": "$4.08","child_month": "$122.40"}, 
                    {"zones": "9", "adult_cash": "$11.50","adult_prepaid": "$9.06", "adult_month": "$271.80","child_cash": "$6.00", "child_prepaid": "$4.53","child_month": "$135.90"}, 
                    {"zones": "10", "adult_cash": "$12.50","adult_prepaid": "$9.96", "adult_month": "$298.80","child_cash": "$6.50", "child_prepaid": "$4.98","child_month": "$149.40"}, 
                    {"zones": "11", "adult_cash": "$14.50","adult_prepaid": "$11.40", "adult_month": "$342.00","child_cash": "$7.50", "child_prepaid": "$5.75","child_month": "$172.50"}, 
                    {"zones": "12", "adult_cash": "$15.50","adult_prepaid": "$12.25", "adult_month": "$367.50","child_cash": "$8.00", "child_prepaid": "$6.15","child_month": "$184.50"}, 
                    {"zones": "13", "adult_cash": "$17.00","adult_prepaid": "$13.51", "adult_month": "$405.30","child_cash": "$9.00", "child_prepaid": "$6.75","child_month": "$202.50"}, 
                    {"zones": "14", "adult_cash": "$18.00","adult_prepaid": "$14.40", "adult_month": "$432.00","child_cash": "$9.50", "child_prepaid": "$7.20","child_month": "$216.00"}
                 ]
    
    return render_template('metlink/tickets-and-fares/index.html', fare_table=fare_table)

@metlink.route('/tickets-and-fares/station-lookup/')
def tickets_and_fares_station_lookup():
    return render_template('metlink/tickets-and-fares/station_lookup.html')
    
@metlink.route('/tickets-and-fares/fare-zones/')
def tickets_and_fares_fare_zones():
    return render_template('metlink/tickets-and-fares/fare_zones.html')
    
@metlink.route('/tickets-and-fares/period-passes/')
def tickets_and_fares_period_passes():
    return render_template('metlink/tickets-and-fares/period_passes.html')
 
@metlink.route('/tickets-and-fares/rail-offpeak/')
def tickets_and_fares_rail_offpeak():
    
    fare_table = [
                    {"zones": "1", "adult_cash": "$2.00","adult_cash_offpeak": "$2.00"}, 
                    {"zones": "2", "adult_cash": "$3.50","adult_cash_offpeak": "$3.00"}, 
                    {"zones": "3", "adult_cash": "$5.00","adult_cash_offpeak": "$4.00"}, 
                    {"zones": "4", "adult_cash": "$5.50","adult_cash_offpeak": "$4.50"}, 
                    {"zones": "5", "adult_cash": "$6.50","adult_cash_offpeak": "$5.00"}, 
                    {"zones": "6", "adult_cash": "$8.00","adult_cash_offpeak": "$6.50"}, 
                    {"zones": "7", "adult_cash": "$9.00","adult_cash_offpeak": "$7.50"}, 
                    {"zones": "8", "adult_cash": "$10.50","adult_cash_offpeak": "$8.50"}, 
                    {"zones": "9", "adult_cash": "$11.50","adult_cash_offpeak": "$9.50"}, 
                    {"zones": "10", "adult_cash": "$12.50","adult_cash_offpeak": "$10.00"}, 
                    {"zones": "11", "adult_cash": "$14.50","adult_cash_offpeak": "$14.50"}, 
                    {"zones": "12", "adult_cash": "$15.50","adult_cash_offpeak": "$15.50"}, 
                    {"zones": "13", "adult_cash": "$17.00","adult_cash_offpeak": "$17.00"}, 
                    {"zones": "14", "adult_cash": "$18.00","adult_cash_offpeak": "$18.00"}
                ]
    
    return render_template('metlink/tickets-and-fares/rail_offpeak.html', fare_table=fare_table)
  
@metlink.route('/tickets-and-fares/school-term-passes/')
def tickets_and_fares_school_term():
    
    fare_table = [
                    {"zones": "1", "school_rail": "$95.00","school_mana": "$120.00", "school_classic": "$85.00","school_tranzit": " -"}, 
                    {"zones": "2", "school_rail": "$119.30","school_mana": "$153.00", "school_classic": "$110.00","school_tranzit": "$135.00"}, 
                    {"zones": "3", "school_rail": "$142.50","school_mana": "$183.00", "school_classic": "$140.00","school_tranzit": "$163.00"}, 
                    {"zones": "4", "school_rail": "$174.80","school_mana": "$225.00", "school_classic": "-","school_tranzit": "$199.00"}, 
                    {"zones": "5", "school_rail": "$206.30","school_mana": "$265.00", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "6", "school_rail": "$237.80","school_mana": "$306.00", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "7", "school_rail": "$273.00","school_mana": "$351.00", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "8", "school_rail": "$306.00","school_mana": " -", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "9", "school_rail": "$339.80","school_mana": " -", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "10", "school_rail": "$373.50","school_mana": " -", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "11", "school_rail": "$431.30","school_mana": " -", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "12", "school_rail": "$461.30","school_mana": " -", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "13", "school_rail": "$506.30","school_mana": " -", "school_classic": " -","school_tranzit": " -"}, 
                    {"zones": "14", "school_rail": "$540.00","school_mana": " -", "school_classic": " -","school_tranzit": " -"}
                ]
    
    return render_template('metlink/tickets-and-fares/school_term.html', fare_table=fare_table)
 
@metlink.route('/tickets-and-fares/transfer-tickets/')
def tickets_and_fares_transfer_tickets():
    return render_template('metlink/tickets-and-fares/transfer_tickets.html')
    
@metlink.route('/tickets-and-fares/other-fares/')
def tickets_and_fares_other_fares():
    return render_template('metlink/tickets-and-fares/other_fares/other_fares.html')
  
@metlink.route('/tickets-and-fares/other-fares/ferry/')
def tickets_and_fares_ferry():
    return render_template('metlink/tickets-and-fares/other_fares/ferry.html')

@metlink.route('/tickets-and-fares/other-fares/airport/')
def tickets_and_fares_airport():
    return render_template('metlink/tickets-and-fares/other_fares/airport.html')
    
@metlink.route('/tickets-and-fares/other-fares/kiwirail-services/')
def tickets_and_fares_kiwirail_services():
    return render_template('metlink/tickets-and-fares/other_fares/kiwirail_services.html')

@metlink.route('/tickets-and-fares/other-fares/cablecar/')
def tickets_and_fares_cablecar():
    return render_template('metlink/tickets-and-fares/other_fares/cablecar.html')

@metlink.route('/tickets-and-fares/other-fares/commuter-buses/')
def tickets_and_fares_commuter_buses():
    return render_template('metlink/tickets-and-fares/other_fares/commuter_buses.html')
  
@metlink.route('/tickets-and-fares/other-fares/after-midnight-buses/')
def tickets_and_fares_after_midnight_buses():
    return render_template('metlink/tickets-and-fares/other_fares/after_midnight_buses.html')
    
@metlink.route('/tickets-and-fares/other-fares/event-tickets/')
def tickets_and_fares_event_tickets():
    return render_template('metlink/tickets-and-fares/other_fares/event_tickets.html')

@metlink.route('/tickets-and-fares/other-fares/supergold/')
def tickets_and_fares_supergold():
    return render_template('metlink/tickets-and-fares/other_fares/supergold.html')
 
  
  
@metlink.route('/tickets-and-fares/how-to-pay-your-fare/')
def tickets_and_fares_how_to_pay_your_fare():
    return render_template('metlink/tickets-and-fares/how_to_pay_your_fare.html')

@metlink.route('/tickets-and-fares/how-to-pay-your-fare/where-to-buy-your-ticket/')
def tickets_and_fares_where_to_buy_your_ticket():
    return render_template('metlink/tickets-and-fares/where_to_buy_your_ticket.html')

@metlink.route('/tickets-and-fares/how-to-pay-your-fare/order-monthly-train-passes/')
def tickets_and_fares_order_monthly_train_passes():
    return render_template('metlink/tickets-and-fares/order_monthly_train_passes.html')
 
  
@metlink.route('/tickets-and-fares/how-to-pay-your-fare/stored-value-cards/')
def tickets_and_fares_stored_value():
    return render_template('metlink/tickets-and-fares/stored_value.html')
     
 
 


@metlink.route('/tickets-and-fares/fare-rules/')
def tickets_and_fares_fare_manual():
    return render_template('metlink/tickets-and-fares/fare_rules.html')
   
@metlink.route('/tickets-and-fares/how-to-pay-your-fare/new-look-tickets/')
def tickets_and_fares_new_look_fares():
    return render_template('metlink/tickets-and-fares/new_look_fares.html')
    
@metlink.route('/tickets-and-fares/how-to-pay-your-fare/changes-to-some-of-our-train-tickets/')
def tickets_and_fares_changes_to_some_of_our_train_tickets():
    return render_template('metlink/tickets-and-fares/changes_to_some_of_our_train_tickets.html')

@metlink.route('/tickets-and-fares/conditions-of-travel/')
def tickets_and_fares_conditions_of_travel():
    return render_template('metlink/tickets-and-fares/conditions_of_travel.html')
  
 
