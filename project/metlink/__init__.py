from flask import Flask
import os
from config import configure_app

app = Flask(__name__)
configure_app(app)    

from .models import *
from .views import *

app.register_blueprint(metlink)