//---- Helper Functions ----//
//Avoiding jQueryt Conflict
Object.defineProperty(Object.prototype, 'html_options', {
	value: function(){
		var options = [];
		// var obj = this;
		for (var key in this) {
			if (this.hasOwnProperty(key)) {
				options.push('<option value="'+key+'">'+this[key]+'</option>');
			}
		}
		return options;
	}, 
	enumerable: false
})


//---- Base UI ----//
var source_data = $('input[name="SourceData"]').data('sources');
var exception_data = $('input[name="ExceptionData"]').data('exceptions');

$('#Line select').on('change', function(e){
	var line = $(this).find('option:selected').val();
	if(line != 0){
		$('#From select').html(source_data[line].html_options().join(''));
		$('#To select').html(source_data[line].html_options().reverse().join(''));
	} else {
		$('#From select').html('<option>- please select a Line -</option>');
		$('#To select').html('<option>- please select a Line -</option>');
	}
});

$('#Line select').trigger('change');

//---- jQuery Validation ----//
$.validator.addMethod('line_allowed', 
	function(val, ele, params){
		var line = $('#Line select').find('option:selected').val();
		var from = $('#From select').find('option:selected').val();
		var to = $('#To select').find('option:selected').val();
		var valid = true;
		if(exception_data !== undefined){
			var exceptions = exception_data[line];
			if(exceptions !== undefined){
				$.each(exceptions['conditions'], function(i, v){
					if(v !== undefined){
						if(v.from == from && v.to == to){
							valid = false;
						} 
					}
				})
			}
		}

		return this.optional(ele) || valid;
	}, 
	$.validator.format("This fare type and line/station combination is not currently available.")
);

$.validator.addMethod('different_stops', 
	function(val, ele, params){
		var from = $('#From select').find('option:selected').val();
		var to = $('#To select').find('option:selected').val();
		valid = true;
		if(from == to){
			valid = false;
		}

		return this.optional(ele) || valid;
	}, 
	$.validator.format("You must select two different stops")
);



$.validator.addMethod('select_value', 
	function(val, ele, params){
		v = $(ele).find('option:selected').val();
		if(v === '- please select a Line -'){
			return false;
		}

		return true;
	}, 
	$.validator.format("This field is required")
);

$.validator.addClassRules('stop-field', {
	line_allowed: true,
	different_stops: true
});

$.validator.addClassRules('dropdown', {
	select_value: true
});


$('.stop-field').on('change', function(){
	$('.stop-field').valid();
})