	
//This is the form!
var form = $('#FareCalulatorForm_FareCalculatorForm');

//Submit this form field change
form.find('select').on('change', function(e){

	//Hide these so if the form is invalid, we aren't still showing some fares for another selection
	$('.results').hide();
	$('.loading').hide();
	if(form.valid()){
		form.submit();
	}
})

//Bind our hanlder
form.bind('submit', function(e){
	submitForm(e);
});

$('.results').hide();
$('.loading').hide();

function submitForm(e){
	e.preventDefault();
	var data = form.serializeObject();

	$('.results').hide();
	$('.loading').show();

	//Api Endpoint
	var url = '/api/v1/FarePrice/'+data.Line+'/'+data.From+'/'+data.To;
	$.get(url, function(json){

		//Template the results
		html = 	'<table class="table">';
		html += 	'<thead>';
		html += 		'<tr>';
		html += 			'<td></td>';
		html += 			'<td>Adult</td>';
		html += 			'<td>Child *</td>';
		html += 		'</tr>';
		html += 	'</thead>';
		html += 	'<tbody>';
		html += 		'<tr>'
		html += 			'<td>Standard</td>';
		html += 			'<td>'+json.AdultSingle+'</td>';
		html += 			'<td>'+json.ChildSingle+'</td>';
		html += 		'</tr>';
		html += 		'<tr>'
		html += 			'<td>Ten Trip</td>';
		html += 			'<td>'+json.AdultTen+'</td>';
		html += 			'<td>'+json.ChildTen+'</td>';
		html += 		'</tr>'
		html += 		'<tr>'
		html += 			'<td>Monthly</td>';
		html += 			'<td>'+json.AdultMonthly+'</td>';
		html += 			'<td>'+json.ChildMonthly+'</td>';
		html += 		'</tr>'
		html += 		'<tr>'
		html += 			'<td>Off Peak</td>';
		html += 			'<td>'+json.OffPeak+'</td>';
		html += 			'<td>'+json.ChildSingle+'</td>';
		html += 		'</tr>';
		html += 	'</tbody>';
		html +='</table>';
		html +='<p class="alert alert-info">* Child fares are available to children aged between 5 and 15 years, and also to full-time secondary students aged between 16 and 19 years with a current ID issued by their school. Children aged 4 years or less travel free.</p>'

		//If a message was included also template this
		if(json.Message.length > 0){
			html += '<p class="alert alert-danger">'+json.Message+'</p>';
		}

		$('.loading').hide();
		$('.results').html(html);
		$('.results').show();
	});
}


//Serialises a form into an object
$.fn.serializeObject = function(){
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};