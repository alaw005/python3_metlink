var SearchDatasets = [];
var MemberDetails;
var map = false;
var geolocation_lat, geolocation_long, geolocation;
var bounds = new google.maps.LatLngBounds();
var geocoder = new google.maps.Geocoder();
var infoBox = {};
infoBox.infoWindow = new google.maps.InfoWindow();
var currentMap;
var defaultLat = -41.28916701;
var defaultLong = 174.77593559;
var wlg = new google.maps.LatLng(defaultLat, defaultLong);
var default_zoom = 15;
var max_auto_zoom_level = 18;
var myStyles = [{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#1d1d2b"}]},{"featureType":"administrative","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"}]},{"featureType":"landscape.man_made","elementType":"geometry","stylers":[{"color":"#e5e5e5"}]},{"featureType":"landscape.natural","elementType":"geometry","stylers":[{"saturation":-100},{"lightness":11}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#cfd0cf"}]},{"featureType":"poi","elementType":"labels.text.fill","stylers":[{"color":"#4a4a4a"}]},{"featureType":"poi","elementType":"labels.text.stroke","stylers":[{"color":"#f9fafa"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"gamma":0.01},{"saturation":-87},{"lightness":53}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.business","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"labels.text.stroke","stylers":[{"visibility":"on"}]},{"featureType":"poi.government","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.medical","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#b2d1ac"}]},{"featureType":"poi.place_of_worship","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"poi.school","elementType":"geometry.fill","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#3c3c3c"},{"visibility":"on"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ededed"},{"visibility":"on"},{"lightness":67}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#c6c5c6"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"on"},{"gamma":0.36},{"lightness":61},{"saturation":-21}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"labels.text.fill","stylers":[{"visibility":"on"}]},{"featureType":"transit.line","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#626262"}]},{"featureType":"transit.line","elementType":"geometry.stroke","stylers":[{"visibility":"off"}]},{"featureType":"transit.line","elementType":"labels.text.fill","stylers":[{"lightness":-12},{"color":"#ffffff"}]},{"featureType":"transit.line","elementType":"labels.text.stroke","stylers":[{"color":"#c6c5c3"},{"visibility":"on"},{"weight":0.6}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#00364A"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#2e688f"}]},{"featureType":"water","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"color":"#cdcdcd"},{"visibility":"off"}]}];
var isBreakpointXS = false;
var isBreakpointSM = false;
var isBreakpointMD = false;
var isBreakpointLG = false;
var isShowingPlan = false;
var activeTab = 'plan';
var isTouch = is_touch_device();
var isSecure = document.location.protocol == "https:";
var mapExpanded = false;
var map_heights = {};
var themeDir = '/themes/metlink2015/';
	
// Define paths to our markers for google maps
var markerA = themeDir+'images/icons/marker-a.png';
var markerA_Round = themeDir+'images/icons/marker-a-round.svg';
var markerB = themeDir+'images/icons/marker-b.png';
var markerB_Round = themeDir+'images/icons/marker-b-round.svg';
var markerVia = themeDir+'images/icons/marker-c.png';
var markerVia_Round = themeDir+'images/icons/marker-c-round.svg';
var markerStop = themeDir+'images/icons/stop.png';

var routeMapMarkerOriginIcon = {
	path: 'M5,10a5,5 0 1,0 10,0a5,5 0 1,0 -10,0',
	scale: 1.5,
	strokeWeight: 6,
	strokeColor: '#0e8394',
	fillColor: '#ffffff',
	fillOpacity: 1,
	anchor: new google.maps.Point(10,10),
};
var routeMapMarkerDestinationIcon = {
	path: 'M5,10a5,5 0 1,0 10,0a5,5 0 1,0 -10,0',
	scale: 1.5,
	strokeWeight: 6,
	strokeColor: '#00364a',
	fillColor: '#ffffff',
	fillOpacity: 1,
	anchor: new google.maps.Point(10,10),
};

var walkCircles = {
	path: 'M-3,0a3,3 0 1,0 6,0a3,3 0 1,0 -6,0',
	strokeWeight: 0,
	scale: 1,
	fillColor: '#00364a',
	fillOpacity: 1,
};

var transitArrow = {
	path: 'M-2.5,-2.5L0 -1.5 2.5 -2.5 2.5 2 0 3 -2.5 2 ',
	strokeWeight: 0,
	scale: 1,
	fillColor: '#c3db09',
	fillOpacity: 1,
	rotation: 180
}
					

function initialize(mapHolder) {
	
	if(!mapHolder){
		console.error('mapHolder was not set');
		return;
	}
	var mapOptions = {
		zoom: default_zoom,
		center: wlg,
		scrollwheel: false,
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		styles: myStyles,
		mapTypeControl: false,
		panControl: false,
		draggable: (isTouch ? false : true),
		zoomControl: (isBreakpointXS != 'xs' ? true : false),
		streetViewControl: false,
		fullscreenControl: false,
		zoomControlOptions: {
		  style: google.maps.ZoomControlStyle.DEFAULT,
		  position: google.maps.ControlPosition.RIGHT_BOTTOM
		},
	};
	
	if(!map_heights[$(mapHolder).attr('id')]){
		map_heights[$(mapHolder).attr('id')] = $(mapHolder).height();
	}
	
	map = new google.maps.Map($(mapHolder).get(0), mapOptions);
	
	// Don't allow infowindows for default POIs
	map.setClickableIcons(false);
	
	//Map is reset so no plan is showing
	isShowingPlan = false;
	
	markersArray = {};
	
	if(isTouch){
		addUnlockLock(map);
	}
}

function is_touch_device() {
	return (('ontouchstart' in window)
      || (navigator.MaxTouchPoints > 0)
      || (navigator.msMaxTouchPoints > 0));
};

function removeUnlockLock(map){
	map.controls[google.maps.ControlPosition.RIGHT_TOP].clear();
}

function addUnlockLock(map){
	// Create the DIV to hold the control
	var unlockControlDiv = document.createElement('div');
	unlockControl = new UnlockLockControl(unlockControlDiv, map);

	unlockControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.RIGHT_TOP].push(unlockControlDiv);
}

function UnlockLockControl(controlDiv, map) {

	// Set CSS styles for the DIV containing the control

	// Set CSS for the control border
	var controlUI = document.createElement('div');
	controlUI.style.backgroundColor = 'white';
	controlUI.style.cursor = 'pointer';
	controlUI.style.textAlign = 'center';
	controlUI.title = 'Unlock';
	controlDiv.appendChild(controlUI);

	// Set CSS for the control interior
	var controlText = document.createElement('div');
	controlText.style.fontSize = '11px';
	controlText.style.lineHeight = '1.8em';
	controlText.style.width = '85px';
	controlText.style.color = '#00364a';
	controlText.style.paddingLeft = '5px';
	controlText.style.paddingRight = '5px';
	controlText.style.paddingTop = '2px';
	controlText.style.paddingBottom = '2px';
	if(!map.draggable){
		controlText.innerHTML = '<img src="/themes/metlink2015/images/icons/map-locked.png" style="width:60px;height:60px;" title="Map Locked" alt="Map Locked"><br />Map Locked';
	}else{
		controlText.innerHTML = '<img src="/themes/metlink2015/images/icons/map-unlocked.png" style="width:60px;height:60px;" title="Map Unlocked" alt="Map Unlocked"><br />Map Unlocked';
	}
	controlUI.appendChild(controlText);

	// Setup the click event listeners: simply set the map to
	google.maps.event.addDomListener(controlUI, 'click', function() {
		if(!map.draggable){
			// Make the map interactive
			map.setOptions({draggable: true});
			$(controlUI).children('div').html('<img src="/themes/metlink2015/images/icons/map-unlocked.png" style="width:60px;height:60px;" title="Map Unlocked" alt="Map Unlocked"><br />Map Unlocked');				
		}else{
			// Make the map non-interactive
			map.setOptions({draggable: false});
			$(controlUI).children('div').html('<img src="/themes/metlink2015/images/icons/map-locked.png" style="width:60px;height:60px;" title="Map Locked" alt="Map Locked"><br />Map Locked');
		}
	});

}

function removeFullScreenOption(map,mapContainer){
	map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].clear();
}

function addFullScreenOption(map,mapContainer){
	var fullPageMapDiv = document.createElement('div');
	new FullPageMapControl(fullPageMapDiv, map, mapContainer);

	fullPageMapDiv.index = 0;
	map.controls[google.maps.ControlPosition.BOTTOM_RIGHT].push(fullPageMapDiv);
}

function FullPageMapControl(controlDiv, map, mapContainer) {

	// Set CSS for the control border
	var controlUI = document.createElement('div');
	controlUI.style.background = 'rgba(255,255,255,0.7)';
	controlUI.style.cursor = 'pointer';
	controlUI.style.textAlign = 'center';
	controlUI.title = 'Full Screen';
	controlDiv.appendChild(controlUI);

	// Set CSS for the control interior
	var controlText = document.createElement('div');
	controlText.style.fontSize = '10px';
	controlText.style.color = 'rgb(68,68,68)';
	controlText.style.paddingLeft = '6px';
	controlText.style.paddingRight = '6px';
	if(mapExpanded){
		controlText.innerHTML = 'Close Full Screen';
	}else{
		controlText.innerHTML = 'Full Screen';
	}
	controlUI.appendChild(controlText);

	// Setup the click event listeners: simply set the map to
	google.maps.event.addDomListener(controlUI, 'click', function() {
		if($(controlUI).children('div').html() == 'Close Full Screen'){
			$(mapContainer).animate({
				height: map_heights[$(mapContainer).attr('id')]
			}, 300, function(){
				recenterMap(map);
			});
			
			mapExpanded = false;
			
			$(controlUI).children('div').html('Full Screen');
		}else{
			var window_height = window.innerHeight ? window.innerHeight : $(window).height();
			var new_height = window_height - $('.navbar').height();
			$(mapContainer).animate({
				height: new_height
			}, 300, function(){
				recenterMap(map);
			});
			
			mapExpanded = true;
						
			$('html, body').animate({
				scrollTop: $(mapContainer).offset().top - ($('.navbar').height() - 1)
			}, 300);
			
			$(controlUI).children('div').html('Close Full Screen');
		}
	});
}
function recenterMap(map){
	var center = map.getCenter();
	google.maps.event.trigger(map, "resize");
	map.setCenter(center); 
}
function fitMapToMarkers(markers, map){
	
	// Clear the bounds to remove any markers that have been since deleted
	var bounds = new google.maps.LatLngBounds();
	
	$.each(markers, function (index, marker) {
		var latlng = marker.getPosition();
		bounds.extend(latlng);
	});
	
	// Don't zoom the map unless we have more than one marker
	if(Object.size(markers) > 1){
		map.fitBounds(bounds);
		
		var listener = google.maps.event.addListener(map, "bounds_changed", function() { 	
			// Set a reasonable max zoom level
			if (map.getZoom() > max_auto_zoom_level){
				map.setZoom(max_auto_zoom_level); 
			}
			google.maps.event.removeListener(listener);
		});

	}
	
}
function humanizeDuration(input, units) { 
	// Ensure we are dealing with positive numbers only
	units = Math.abs(units)
	
	// units is a string with possible values of y, M, w, d, h, m, s, ms
	var duration = moment().startOf('day').add(units, input),
		format = "";

	if(duration.hour() > 0){ format += "H[h] "; }
	if(duration.minute() > 0){ format += "m[m] "; }
	format += "s[s]";

	return duration.format(format);
}
function stopDepartureTemplate(data, limit){
	limit = typeof limit !== 'undefined' ? limit : 12;
	
	var d = '<div class="rt-info-box" data-rt-stop-id="'+data.Stop.Sms+'" data-rt-current-time="'+moment(data.LastModified).format('h:mma')+'">';
		if(data.Stop){
			d += '<a href="/my-metlink" class="stop-bookmark pull-right"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span></a>';
			d += '<div class="rt-stop-heading clearfix">';
				if(data.Stop.Icon){
					d += '<a href="stop/'+data.Stop.Sms+'"><img class="stop-image-sm" alt="Stop '+data.Stop.Sms+'" src="'+data.Stop.Icon+'" /></a>';
				}						
				d += '<small>Stop '+data.Stop.Sms+'</small>';
				d += '<h4 class="rt-stop-name"><a href="stop/'+data.Stop.Sms+'">'+data.Stop.Name+'</a></h4>';
			d += '</div>';
		}
		
		if(data.Notices){
			limit = limit - 2;
			d += '<div class="alert alert-danger line-note">';
			$.each(data.Notices, function(index, notice){
				d += '<p><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '+(notice.LineRef ? '(#'+notice.LineRef+')' : '')+notice.LineNote+'</p>';
			});
			d += '</div>';
		}
		
		if(data.NoticesClosures){
			d += '<div class="rt-notices">\
				<div class="alert alert-danger line-note">';
			$.each(data.NoticesClosures, function(index, notice){
				d += '<p><span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span> '+(notice.LineRef ? '(#'+notice.LineRef+')' : '')+notice.LineNote+'</p>';
			});
			d += '</div>\
			</div>';
		}else{	
			d += '<div class="rt-info-content">';
			if(data.Services){
				d += '<div class="table-responsive">\
					<table aria-label="'+data.Stop.Name+'" class="table table-condensed table-hover">';
					
						var lastDate = moment(data.LastModified).format('YYYY-MM-DD');
						$.each(data.Services,function(index,service){
							var thisDate = moment(service.DisplayDeparture).format('YYYY-MM-DD');
							if(lastDate && thisDate != lastDate){
								d += '<tr class="rowDivider"><td colspan="4">'+moment(service.DisplayDeparture).format('ddd Do MMM')+'</td></tr>';
							}
							lastDate = thisDate;
							
							d += '<tr class="'+(moment(service.DisplayDeparture).diff(moment(data.LastModified), 'minutes') <= 6 ? 'active ':'')+''+(moment(service.DisplayDeparture).diff(moment(data.LastModified), 'minutes') <= 2 ? 'rt-due-row':'')+'">\
								<td class="text-center routeNumber" style="width:35px;"><a href="'+service.Service.Link+'" class="id-code-link" title="View full timetable for the '+service.Service.TrimmedCode+' route" '+(!service.HasWheelchair ? 'aria-label="Route '+service.Service.TrimmedCode+' -  This service is not wheelchair accesible"':'')+'>'+service.Service.TrimmedCode+'</a></td>\
								<td><a href="'+service.Service.Link+'" title="View full timetable for the '+service.Service.TrimmedCode+' route" class="rt-service-destination">'+(service.DepartureStatus == 'cancelled' ? '<span  class="rt-destination-name cancelled-service">CANCELLED</span>':'<span class="rt-destination-name">'+service.DestinationStopName+'</span>')+'</a></td>\
								<td class="text-center '+(moment(service.DisplayDeparture).diff(moment(data.LastModified), 'minutes') <= 2 ?'rt-due':'')+'" style="width:80px;">'+((service.IsRealtime && service.DepartureStatus != 'cancelled') ? '<span class="rt-service-time real" title="Based on the actual location of the service">'+(moment(service.DisplayDeparture).diff(moment(data.LastModified), 'minutes') <= 2 ? 'Due':moment(service.DisplayDeparture).diff(moment(data.LastModified), 'minutes')+' mins')+'</span>':'<span tabindex="0" class="rt-service-time" title="Based on the scheduled services. No real time updates are available.">'+moment(service.DisplayDeparture).format('h:mma')+'</span>')+'</td>\
								<td class="text-center" style="width:40px;">';
									if(service.Note){ d += '<a class="tooltipStd" href="#note-'+service.Note.Key+'" data-toggle="tooltip" title="'+service.Note.Text+'">'+service.Note.Key+'</a>'; }
									if(service.VehicleFeature == "lowFloor"){ d += '<span tabindex="0" data-toggle="tooltip" class="wheelchair-holder" aria-label="This service has low floors and is wheelchair accessible" title="This service has low floors"><img src="'+themeDir+'images/icons/wheelchair_round.svg" alt="" class="svg-coloured" onerror="this.src=\''+themeDir+'images/icons/wheelchair.png\'; this.onerror=null;" /></span>'; }
								d += '</td>\
							</tr>';
							if(index+1 >= limit){ return false; }
						});
					d += '</table>\
				</div>';
			}else{
				d += '<p tabindex="0">No live departures available, please refer to timetable information.</p>';
			}
			d += '</div>';
		}
	d += '</div>';
	
	return d;
}

var membercall;
function loadMemberDetails(callback) {
	if(!membercall) {
		membercall = $.get('/api/MemberDetails', function(){}, "json").promise();
	} 
	membercall.done(callback);
	membercall.fail(callback);
}

$(document).ready(function() {

	// Set MemberDetails array to member details firstly
	loadMemberDetails(saveMemberDetails);
	
	function saveMemberDetails(Member){
		if(Member.Exists){
			MemberDetails = Member;
		}
	}
	
	var LoginForm;
	$('#LoginFormDropdown').on('show.bs.dropdown', function () {

		if(!LoginForm) {
			LoginForm = $.get('/api/LoginForm', function(){}, "html").promise();
		} 
		LoginForm.done(function(data){
			$("#LoginFormDropdown .dropdown-contents").removeClass('loading').html(data);
		});
		LoginForm.fail(function(){
			'Login details are currently unavailable.';
		});
		
	});
	
	if (typeof updateHeaderArea == 'function') { 
		loadMemberDetails(updateHeaderArea);
	}
	
	function updateHeaderArea(Member){
		if(Member.Exists){
			$(".sign-in-options").html('Hi '+Member.FirstName+'<br /><a href="/my-metlink" title="My Metlink">My Metlink</a> | <a href="/Security/logout" title="Log out">Log out</a>');
			
			// Replace the mobile menu Sign In / Create an Account options
			$("#mobile-member-signin").html('<a href="/my-metlink" title="My Metlink">My Metlink</a>');
			$("#mobile-member-register").html('<a href="/Security/logout" title="Log out">Log out</a>');
		}
	}
	
	if(/iPhone|iPod|Android|iPad/.test(window.navigator.platform)){
		$(document).on('focus', 'textarea,input,select', function(e) {
			$('.navbar.navbar-fixed-top').css('position', 'absolute');
		}).on('blur', 'textarea,input,select', function(e) {
			$('.navbar.navbar-fixed-top').css('position', '');
		});
	}
	
	$('body').scrollspy({ 
		target: '#scrollspynav',
		offset: $('nav').height() + 1
	});


	/*--- Adding hover and delay to dropdowns ---*/
	/* Stolen from bootstrap.js */
	var backdrop = '.dropdown-backdrop';
	var toggle   = '[data-toggle="dropdown"], .data-dropdown-toggle';
	/* Stolen from bootstrap.js */
	function getParent($this) {
	    var selector = $this.attr('data-target')

	    if (!selector) {
	      selector = $this.attr('href')
	      selector = selector && /#[A-Za-z]/.test(selector) && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
	    }

	    var $parent = selector && $(selector)

	    return $parent && $parent.length ? $parent : $this.parent()
	}
	/* Stolen from bootstrap.js */
	function clearMenus(e) {
	    if (e && e.which === 3) return
	    $(backdrop).remove()
	    $(toggle).each(function () {
	      var $this         = $(this)
	      var $parent       = getParent($this)
	      var relatedTarget = { relatedTarget: this }

	      if (!$parent.hasClass('open')) return

	      if (e && e.type == 'click' && /input|textarea/i.test(e.target.tagName) && $.contains($parent[0], e.target)) return

	      $parent.trigger(e = $.Event('hide.bs.dropdown', relatedTarget))

	      if (e.isDefaultPrevented()) return

	      $this.attr('aria-expanded', 'false')
	      $parent.removeClass('open').trigger('hidden.bs.dropdown', relatedTarget)
	    })
	 }
	
	var close;
	$(document).on({
		mouseenter: function() {
			if( !isBreakpoint('xs') ) { 
				clearMenus();
				clearTimeout(close);
				$(this).trigger('show.bs.dropdown');
				$(this).addClass('open');
			}
		}, mouseleave: function(e) {
			if (!$(e.target).is('input')) {
				if( !isBreakpoint('xs') ) { 
					$(this).trigger('hide.bs.dropdown');
					var self = $(this);
					current = self;
					close = setTimeout(function(){
						self.removeClass('open');
					}, 400);
					
				} 
			}
		}
	}, '.navbar-primary .dropdown');

	/* -------- */



	//For accessiblity we need to close dropdown menus when we tab out of them.
	//The focus pseudo selector on .focusout doesn't work in this way, so we detect 
	//key down and key ups and determine two things:
	// 1. The element we are tabbing off is a child of a dropdown
	// 2. The element we are tabbing to is not a sibling of that same dropdown
	var prevTabbed;
	var currentTabbed;
	$(document).on({
		keydown: function (e){
			if(e.which == 9){
				prevTabbed = $(document.activeElement);
			}
		},
		keyup: function (e){  
			if(e.which == 9){
				currentTabbed = $(document.activeElement);
				if(	isDropdownChild(prevTabbed) && !isDropdownChild(currentTabbed)){
					clearMenus();
				}
			}
		}
	});

	//Made a function to make things a littl tidier.
	function isDropdownChild(ele){	

		return ele !== undefined && ele.parents('.dropdown-menu').length;
	}
	
	$(".homepage-carousel a.btn").on('click',function(e){
		if (typeof ga === 'function') {
			ga('send', 'event', {
				eventCategory: 'Homepage Jumbotron',
				eventAction: 'click',
				eventLabel: $(this).parent('div').children('h2').text(),
				transport: 'beacon'
			});
		}
	});
		
	$("#carousel-primary").carousel({
		interval : 8000,
		pause: "hover",
	});
	
	$("#carousel-footer").carousel({
		interval : 8000,
		pause: "hover",
	});
	
	/* ---------------------------------------
	 * TIMETABLES (HOMEPAGE)
	 * ------------------------------------- */
	 
	// Timetables Accordion on homepage
	$('#timetableModes').on('shown.bs.collapse', function (e) {
		$('.panel-collapse.in').siblings('.timetableRow').find('.glyphicon').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
		var offset = $('.panel.panel-primary > .panel-collapse.in').offset();
		if(offset) {
            $('html,body').animate({
				scrollTop: $('.panel-collapse.in').siblings('.timetableRow').offset().top - $('nav').height() + 1
            }, 250); 
        }
    }); 
    $('#timetableModes').on('hide.bs.collapse', function (e) {
    	$('.panel-collapse.in').siblings('.timetableRow').find('.glyphicon').toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
    });
	
	//Autofocus on form error message for RouteTimetableInput
 	if($('#Form_RouteTimetableInput_message').length > 0){
 		$('html,body').scrollTop( $("a[name=timetables]").offset().top );  
 		$('#Form_RouteTimetableInput_message').focus();
 	}
	
	if($('#timetableModes').length){
		var timetablesData = [];
		$("#timetableModes .containerTimetableList table tr").each(function(index){
			timetablesData.push({
				"Link": $(this).children('td.routeNumber').children('a').attr('href'), 
				"TrimmedCode": $(this).children('td.routeNumber').text(), 
				"Name": $(this).children('td').not('.routeNumber').text()
			});
		});
		
		var timetablesSource = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('TrimmedCode','Name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			identify: function(obj){ return obj.TrimmedCode; },
			local: timetablesData
		});

		$('#Form_RouteTimetableInput input#route').typeahead({
			hint: false,
			highlight: true,
			minLength: 1
		},{
			display: 'TrimmedCode',
			limit: 10,
			templates: {
				suggestion: function (json) {
					return '<div>' + json.TrimmedCode + ' - ' + json.Name + '</div>';
				}
			},
			source: timetablesSource
		});
		
		$('#Form_RouteTimetableInput input#route').bind('typeahead:select',function(ev,suggestion){
			$('#Form_RouteTimetableInput input.action').click();
		});
	}else{
		// If we dont have a local source of timetableData, then get it from the API
		var timetablesSource = new Bloodhound({
			datumTokenizer: Bloodhound.tokenizers.obj.whitespace('TrimmedCode','Name'),
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			identify: function(obj){ return obj.TrimmedCode; },
			prefetch: 'api/v1/ServiceList/',
			initialize: false
		});
		
		// Set up a handler so as soon as we focus on a search form we initialize rather than on page load
		$("#SearchForm_SearchForm input#SearchForm_SearchForm_Search").on('focus',function(e){
			timetablesSource.initialize();
		});
	}
	
	// Add timetables to our primary search suggestions (uses either local or api data)
	SearchDatasets.push({
		display: 'Name',
		limit: 10,
		templates: {
			header: '<strong class="tt-subheading">Timetables</strong>',
			suggestion: function (json) {
				return '<a href="'+json.Link+'">' + json.TrimmedCode + ' - ' + json.Name + '</a>';
			}
		},
		source: timetablesSource
	});
	
	if (typeof updateFavouriteServices == 'function') { 
		loadMemberDetails(updateFavouriteServices);
	}

	function updateFavouriteServices(Member){
		var savedServicesDropdown = $('ul#Form_RouteTimetableInput_SavedServices').first();
		if(Member.Exists && Member.Services.length >= 1 && savedServicesDropdown.length){
			// Clear the dropdown
			savedServicesDropdown.html('');
			$.each(Member.Services, function(key, service){
				$('<li><a href="'+service['Link']+'">' + service['Code'] + ' - ' + service['Name'] + '</a></li>').appendTo(savedServicesDropdown);
			});
		}else{
			// Potentially load latest services by cookie!
		}
	}

	// Global form validation settings

	$.validator.addMethod("checkUppercase", function(value, element) {
	   return /[A-Z]/.test(value);
	}, 'Password must contain atleast one uppercase letter');

	$.validator.addMethod("checkLowercase",	function(value, element) {
		return hasLowerCase = /[a-z]/.test(value);
	}, 'Password must contain atleast one lowercase letter');

	$.validator.addMethod("checkNumbers", function(value, element) {
	   return hasNumbers = /\d/.test(value);
	}, 'Password must contain atleast one number');
	
	$.validator.addMethod( "pattern", function( value, element, param ) {
		if ( this.optional( element ) ) {
			return true;
		}
		if ( typeof param === "string" ) {
			param = new RegExp( "^(?:" + param + ")$" );
		}
		return param.test( value );
	}, "Sorry that doesn't appear to be a correct format" );

	$(".validate").each(function() {
		$(this).validate({
			rules: {
				'Password[_Password]': {
					required: true,
					minlength: 8,
					checkUppercase: true,
					checkLowercase: true,
					checkNumbers: true
				},
				'NewPassword1': {
					required: true,
					minlength: 8,
					checkUppercase: true,
					checkLowercase: true,
					checkNumbers: true
				}		
			},
			highlight: function(element) {
				$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
				$(element).siblings('.glyphicon').removeClass('glyphicon-ok').addClass('glyphicon-remove');
			},
			unhighlight: function(element) {
				$(element).closest('.form-group').removeClass('has-error').addClass('has-success');
				$(element).siblings('.glyphicon').removeClass('glyphicon-remove').addClass('glyphicon-ok');         
			},
			errorElement: 'span',
			errorClass: 'help-block',
			errorPlacement: function(error, element) {
				if(element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else if($(element).closest('.input-group').length){
					error.insertAfter($(element).closest('.input-group'));
				} else {
					error.insertAfter(element);
				}
			} 
		});
	});

	
	/* ---------------------------------------
	 * PLAN & EXPLORE SWITCHING
	 * ------------------------------------- */
	
	$('.heading-tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		
		// Switch our map styles, and display/remove required pins
		switch(e.target.hash){
			case '#explore-tab':
			
				activeTab = 'explore';
				
				// Remove elements no longer required and reset any vars
					// To keep: From marker
					// Remove: Journey Plan results, To marker, Via marker
				
				// Reset panel
				// Clear the results tab
				$("#journey-planner-results-holder").hide();
				$("#journey-planner-results-holder").html();
				
				// Display the options tab
				$("#journey-planner-options-holder").show();				
				
				// Reset the Vehicle Map if we have it
				if (typeof resetVehicleLocationMap == 'function') { 
					resetVehicleLocationMap();
				}
		
				// Hide all Markers (except "From") and Route overlays (JSON) (if we have any)
				hideRoutes();
				hideAllMarkers([journeyFields.journeyOrigin._Field]);		
				
				// Disable the mouse click handler for context menu?
				google.maps.event.clearListeners(map,'click');
				
				// Disable the ability to drag markers on Locate
				$.each(markersArray, function (index, marker) {
					marker.setDraggable(false);
				});
			
				// Set our Style
				var mapOptions = { minZoom:1};
				map.setOptions(mapOptions);
				
				// Set category dropdown back to all
				$('form[name="changeExploreCategory"] select').val('all');
	
				$('#plan-helper-text').hide();
				$('#locate-helper-text').show();
				
				// Check if we are on mobile
				// If so, expand Locate map to fit
				if(isPlanBreakpointXS){
					
					// Remove the Expand/Collapse Button
					removePlanExpandCollapse();
					
					// Add a Locate Close button
					addLocateCollapse();
					
					// Expand the Map
					expandLocateMap();
				}
				
				// Load in the POI and place on map
				// Probably best done with ajax call to return json data from POI DB
				if(exploreLocations){
					addExploreLocations(exploreLocations);
				}else{
					$.get('api/ExploreLocationsJson', function(data) {
						// Store this data so we dont need to do it again if we switch back to Plan and back again
						exploreLocations = data;
						
						addExploreLocations(exploreLocations);
					}, "json" );
				}
			
				break;
			case '#journey-tab':
			
				activeTab = 'plan';
				
				// Remove POI pins - this also removes a infoWindow if visible
				hideAllExploreMarkers();
				if(markerCluster){	
					markerCluster.clearMarkers();
				}
				
				$('#locate-helper-text').hide();
				$('#plan-helper-text').show();
					
				// Show all makers again so we're back to like nothing ever happened..
				showAllMarkers()
				
				// Re-enable any journey plan results or removed markers that were there??
				addClickListener();
				
				// Re-enable the ability to drag markers
				$.each(markersArray, function (index, marker) {
					marker.setDraggable(true);
				});
				
				// Check if we are on mobile
				// If so, expand Locate map to fit
				if(isPlanBreakpointXS){
					// Remove the Expand/Collapse Button
					removeLocateCollapse();
					
					// Force back to small size
					collapsePlanMap();
					
					// Add Plan Expand/Close button
					addPlanExpandCollapse();
				}
				
				// Reset the Vehicle Map if we have it
				if (typeof resetVehicleLocationMap == 'function') { 
					resetVehicleLocationMap();
				}
				
				break;
		}
	});
	
		
	/* ---------------------------------------
	 * SEARCH
	 * ------------------------------------- */
	$('#Search input.text')
		.focus(function() {
			value = $(this).val();
			if (value == "Search") {
				$(this).val('');
				$(this).removeClass('init');
			}
		}).blur(function() {
			value = $(this).val();
			if (value === "") {
				$(this).addClass('init');
				$(this).val('Search');
			}
		}).addClass('init');
	
	var searchTypeaheadTimeout;
	// Add Stop datasets to Search
	SearchDatasets.push({
		display: 'Sms',
		limit: 10,
		async: true,
		templates: {
			header: '<strong class="tt-subheading">Stops</strong>',
			suggestion: function (json) {
				return '<a href="stop/'+json.Sms+'">Stop ' + json.Name + '</a>';
			}
		},
		source: function (query, processSync, processAsync) {
			if(query.length < 3){ return; }
			
			if (searchTypeaheadTimeout) {
				clearTimeout(searchTypeaheadTimeout);
			}

			searchTypeaheadTimeout = setTimeout(function() {
				return $.ajax({
					url: '/api/v1/StopSearch/'+query.toUpperCase(), 
					type: 'GET',
					dataType: 'json',
					success: function (json) {
						return processAsync(json);
					}
				});
			}, 300);
		}
	});
	
	// Search auto-suggest
	$("#SearchForm_SearchForm input#SearchForm_SearchForm_Search").typeahead({
		hint: false,
		highlight: true,
		minLength: 1
	},SearchDatasets);	
	
	// Set up of tooltips	
	$('body').tooltip({ selector: '[data-toggle=tooltip]' });
		
	
	// Collapsable menu on small screens
	function toggleChevron(e) {
		$(e.target)
			.prev('.panel-heading')
			.find('span.glyphicon')
			.toggleClass('glyphicon-chevron-down glyphicon-chevron-up');
	}

	$('.sidebar div.panel-body').on('show.bs.collapse', toggleChevron);
	$('.sidebar div.panel-body').on('hide.bs.collapse', toggleChevron);

	
	/* ---------------------------------------
	 * RESIZE TASKS
	 * ------------------------------------- */
	
	doBreakpointResizeTasks();
	
	$(window).resize(function(){
		doBreakpointResizeTasks();
	});
	
	function doBreakpointResizeTasks(){
		if( isBreakpoint('xs') ) {
			// Only perform these actions on breakpoint changes!
			if( isBreakpointXS != 'xs'){
				// Sidebar panels
				$('.sidebar .panel-body').removeClass('in');
				
				// Fullimagetease
				$('.section-block-fullimagetease').height($('.fullimagetease-caption').height() + 40);
				$('.section-block-fullimagetease').find('div.item').height($('.section-block-fullimagetease').height());
				
				// Resize the timetable if there is one
				if($('#timetableData').length){
					$("#timetableDataStops tbody tr th.stop").css('white-space','normal').css('max-width','150px');
					
					$("#timetableData tbody").find('tr').each(function(){ 
						$(this).height( $('#timetableDataStops tbody tr').eq($(this).index()).height() );
					});
				}
				
				if(map){
					map.setOptions({zoomControl: false});
				}
				
				isBreakpointXS = 'xs';
				expandColumns();			
			}
		}else{
			// Remove the isBreakpointXS if not valid anymore
			if( isBreakpointXS == 'xs'){
				$('.sidebar .panel-body').addClass('in').css('height','auto');
				
				// Resize the timetable back to normal
				if($('#timetableData').length){
					$("#timetableDataStops tbody tr th.stop").css('white-space','').css('max-width','');
					$("#timetableData tbody").find('tr').each(function(){ 
						$(this).css('height','auto');
					});
				}
				
				if(map){
					map.setOptions({zoomControl: true});
				}
				
				isBreakpointXS = false;
			}
		}
		
		if(isBreakpoint('sm')){
			// Perform these actions every resize
			$('.section-block-fullimagetease').height($('.section-block-recentnews').height());
			$('.section-block-fullimagetease').find('div.item').height($('.section-block-fullimagetease').height());
			
			// Only perform these actions on breakpoint changes!
			if( isBreakpointSM != 'sm'){
				isBreakpointSM = 'sm';
				collapseColumns();
			}
		}else{
			// Remove the isBreakpointSM if not valid anymore
			if( isBreakpointSM == 'sm'){
				isBreakpointSM = false;
			}
		}
		
		if(isBreakpoint('md')){
			// Perform these actions every resize
			$('.section-block-fullimagetease').height($('.section-block-recentnews').height());
			$('.section-block-fullimagetease').find('div.item').height($('.section-block-fullimagetease').height());
			
			// Only perform these actions on breakpoint changes!
			if( isBreakpointMD != 'md'){
				isBreakpointMD = 'md';
				collapseColumns();
			}
		}else{
			// Remove the isBreakpointMD if not valid anymore
			if( isBreakpointMD == 'md'){
				isBreakpointMD = false;
			}
		}
		
		if(isBreakpoint('lg')){
			// Perform these actions every resize
			$('.section-block-fullimagetease').find('div.item').css('height','auto');
			$('.section-block-fullimagetease').find('div.item').height($('.section-block-fullimagetease').height());
			
			// Only perform these actions on breakpoint changes!
			if( isBreakpointLG != 'lg'){
				// Fullimagetease
				$('.section-block-fullimagetease').css('height','auto');
				
				isBreakpointLG = 'lg';
				expandColumns();
			}
		}else{
			// Remove the isBreakpointLG if not valid anymore
			if( isBreakpointLG == 'lg'){
				//$('.section-block-fullimagetease').css('height','auto');
				isBreakpointLG = false;
			}
		}
	}


	function collapseColumns(){
		for(var i = 0; i < $('.containerTimetableList').length; i++){
			var ele = $('.containerTimetableList')[i];
			var col_1 = $(ele).find('.col-1');
			var col_2 = $(ele).find('.col-2');
				var col_3 = $(ele).find('.col-3');
				var col_4 = $(ele).find('.col-4');
				var col_2_content = col_2.html();
				col_2.html('');
				col_1.append(col_2_content);
		};
	}

	function expandColumns(){
		for(var i = 0; i < $('.containerTimetableList').length; i++){
			var ele = $('.containerTimetableList')[i];
			var col_1 = $(ele).find('.col-1');
			var col_2 = $(ele).find('.col-2');
			var col_3 = $(ele).find('.col-3');
			var col_4 = $(ele).find('.col-4');
			
			var tables = col_1.find('table')
			if(tables.length > 1){
				col_2.append(tables[1]);
			}
		};
	}

	//Smooth scrolling
	$('a').each(function(i){
		var name = $(this).attr('name');
		if (typeof name !== typeof undefined && name !== false && !$(this).hasClass('anchor') && $(this).html().length < 1) {
		   $(this).addClass('anchor');
		}
	})

	$('a[href*=#]:not([href=#]):not([href="/#"]):not([role=tab]):not([role=button]):not([data-toggle]):not("#skipLink")').click(function(e) {
		var anchor = $(this);
		if( $(e.target).is('a') && ( $(e.target).attr('class') != 'dropdown-toggle' ) ) {
			$('.navbar-collapse.in').collapse('hide');
		}
    	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
	      	var target = $(this.hash);
	      	target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
	      	if (target.length) {
	        	$('html,body').animate({
	      		scrollTop: target.offset().top
	    		}, 500, function(){
	    			window.location = anchor.attr('href');

	    		});
	        	return false;
	      	}
	    }
 	});

 	//Skip to main content focus shifting handler
 	$('#skipLink').on('click', function(e){
 		//Prevent the default click
 		e.preventDefault();

 		//Get the href we need
 		var href = $(this).attr('href');
 		//Slice off the leading slash
 		var parts = href.split('#');	
 		var target = '#'+parts[1];	
 		//Scroll and focus	
 		$('html,body').scrollTop( $(target).offset().top);  
 		$(target).focus();
 		//If we didn't gain focus then find the first <a> in the targetted element 
 		//and focus on that
 		if($(':focus').attr('id') == 'skipLink'){
 			$(target).find('a:first').focus();
 		}
 	});

 	//Key press handler for collapse
 	$('.timetableRow a').on('keypress', function(e){
		if(e.which == 13) { 
			var target = $(this).data('target');
			$(target).collapse('toggle');
		}
	});

	//Key press handler for special checkboxes
 	$('.howCheckbox').on('keypress', function(e){
 		e.preventDefault();
		if(e.which == 32) {
			var input = $(this).find('input')
			input.prop("checked", !input.prop("checked"));
		}
	});

	//Focus for mobile nav
	$('.mobile-search, .dropdown.search').on('shown.bs.dropdown', function(){
		$(this).find('input[type=text]').focus();
	});
  
});

/*
* Replace all SVG images with inline SVG
*/

embedSVGs = function(){

	jQuery('img.svg-coloured').each(function(){
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			
			// Add preserveaspectratio
			$svg = $svg.attr('preserveaspectratio','xMidYMid meet');
			
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}

			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');
			
			// Always ensure we set a MAX-HEIGHT on SVGs that are embedded
			// This stops some browsers breaking on height, but allows them to still be responsive scaling
			// MAX-HEIGHT MUST be set on SVG, not parent container (can be both)
			$svg = $svg.attr('width','100%');
			$svg = $svg.attr('height','100%');

			// Replace image with new SVG
			$img.replaceWith($svg);

		}, 'xml');

	});
}
embedSVGs();

Object.size = function(obj) {
	var size = 0, key;
	for (key in obj) {
		if (obj.hasOwnProperty(key)) size++;
	}
	return size;
};
function isBreakpoint( alias ) {
    return $('.device-' + alias).is(':visible');
}