import os

basedir = os.path.abspath(os.path.dirname(__file__))

class BaseConfig(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = 'Keep me secret'

class DevelopmentConfig(BaseConfig):
    DEBUG = True
    TESTING = False

class TestingConfig(BaseConfig):
    DEBUG = True
    TESTING = True

config = {
    "default": "config.BaseConfig",
    "development": "config.DevelopmentConfig",
    "testing": "config.TestingConfig"
}

def configure_app(app):
    """Allow specification of alternative configuration settings.
    Use environment variable to specify, eg to use development config:
       export FLASK_CONFIGURATION = 'development' 
    NB use "set" not "export" in Windows
    """
    config_name = os.getenv('FLASK_CONFIGURATION', 'default')
    app.config.from_object(config[config_name])
