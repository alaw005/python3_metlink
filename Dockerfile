FROM python:3.4-alpine
COPY project /project
WORKDIR /project
RUN pip3 install -r requirements.txt
CMD [ "python3", "run.py" ]
